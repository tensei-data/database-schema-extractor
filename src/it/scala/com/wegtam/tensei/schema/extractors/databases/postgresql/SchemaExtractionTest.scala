/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors.databases.postgresql

import java.nio.charset.StandardCharsets
import java.net.URI
import java.sql.{ Connection, DriverManager }

import cats.effect._
import cats.effect.unsafe.implicits.global
import com.wegtam.tensei.schema.extractors._
import com.wegtam.tensei.schema.extractors.IntegrationSpec
import com.wegtam.tensei.schema.extractors.PrettyPrinterInstances._
import com.wegtam.tensei.schema.extractors.PrettyPrinter.ops._
import com.wegtam.tensei.schema.extractors.databases._
import eu.timepit.refined.auto._
import org.xmlunit.builder._

import scala.util.Try

class SchemaExtractionTest extends IntegrationSpec {

  private val databaseHost = Option(System.getenv("POSTGRESQL_HOST")).getOrElse("localhost")
  private val databasePort = Option(System.getenv("POSTGRESQL_PORT")).getOrElse("5432")
  private val databaseName = "test_extraction"
  private val databaseUser = Option(System.getenv("POSTGRESQL_USER")).getOrElse("tensei-it")
  private val databasePass =
    Option(System.getenv("POSTGRESQL_PASS")).getOrElse("DEFAULT_DB_PASSWORD")

  /** Disable security manager to avoid getting a
    * `java.security.AccessControlException` while testing.
    * Also we explicitly load the required JDBC driver because auto loading
    * somehow rarely works reliable.
    */
  override protected def beforeAll(): Unit = {
    System.setSecurityManager(null)
    val _ = Class.forName("org.postgresql.Driver")
    val connection = DriverManager
      .getConnection(
        s"jdbc:postgresql://$databaseHost:$databasePort/template1",
        databaseUser,
        databasePass
      )
    val s = connection.createStatement()
    s.execute(s"DROP DATABASE IF EXISTS $databaseName")
    s.close()
    connection.close()
  }

  /** Create a fresh database.
    */
  override protected def beforeEach(): Unit = {
    val connection = DriverManager
      .getConnection(
        s"jdbc:postgresql://$databaseHost:$databasePort/template1",
        databaseUser,
        databasePass
      )
    val s = connection.createStatement()
    s.execute(
      s"SELECT pid, pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = current_database() AND pid <> pg_backend_pid()"
    )
    s.execute(s"CREATE DATABASE $databaseName")
    s.close()
    connection.close()
    super.beforeEach()
  }

  /** Delete the test database after each test.
    */
  override protected def afterEach(): Unit = {
    val connection = DriverManager
      .getConnection(
        s"jdbc:postgresql://$databaseHost:$databasePort/template1",
        databaseUser,
        databasePass
      )
    val s = connection.createStatement()
    s.execute(s"REVOKE CONNECT ON DATABASE $databaseName FROM public")
    s.execute(
      s"SELECT pid, pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = '$databaseName' AND pid <> pg_backend_pid()"
    )
    s.execute(s"DROP DATABASE IF EXISTS $databaseName")
    s.close()
    connection.close()
    super.afterEach()
  }

  /** Create the test tables within the database.
    *
    * @param connection A connection to the database.
    */
  private def createDatabaseTables(connection: Connection): Unit = {
    val s = connection.createStatement()
    s.execute("""
        |CREATE TABLE salary_groups (
        |  id SERIAL,
        |  name VARCHAR(64) NOT NULL,
        |  min_wage DECIMAL(8,2) NOT NULL,
        |  max_wage DECIMAL(8,2) NOT NULL,
        |  PRIMARY KEY (id),
        |  UNIQUE (name)
        |)
      """.stripMargin)
    s.execute("""
        |CREATE TABLE employees (
        |  id BIGSERIAL,
        |  firstname VARCHAR(64) NOT NULL,
        |  lastname VARCHAR(64) NOT NULL,
        |  birthday DATE NOT NULL,
        |  notes TEXT,
        |  salary_group INT REFERENCES salary_groups (id),
        |  search TSVECTOR,
        |  PRIMARY KEY (id)
        |)
      """.stripMargin)
    s.execute("""CREATE INDEX employees_search_idx ON employees USING GIN (search)""")
    s.execute("""
        |CREATE TABLE wages (
        |  employee_id BIGINT REFERENCES employees (id),
        |  salary_group INT REFERENCES salary_groups (id),
        |  wage DECIMAL(6,2) NOT NULL,
        |  PRIMARY KEY (employee_id, salary_group)
        |)
      """.stripMargin)
    s.execute("""
        |CREATE TABLE company_cars (
        |  id SERIAL,
        |  license_plate VARCHAR(16) NOT NULL,
        |  employee_id BIGINT REFERENCES employees (id),
        |  seats SMALLINT,
        |  bought DATE,
        |  PRIMARY KEY (id),
        |  UNIQUE (license_plate)
        |)
      """.stripMargin)
    s.execute("""
        |CREATE TABLE parking_slots (
        |  id SERIAL,
        |  employee_id BIGINT REFERENCES employees (id),
        |  license_plate VARCHAR(16),
        |  PRIMARY KEY (id),
        |  UNIQUE (license_plate)
        |)
      """.stripMargin)
    s.close()
  }

  // Expected extraction result.
  val expectedDfasdlContent =
    """
      |<?xml version="1.0" encoding="UTF-8" standalone="no"?>
      |<dfasdl xmlns="http://www.dfasdl.org/DFASDL" semantic="custom">
      |    <seq db-primary-key="id" id="company_cars">
      |        <elem id="company_cars_row">
      |            <num db-auto-inc="true" db-column-name="id" id="company_cars_row_id" max-digits="32"/>
      |            <str db-column-name="license_plate" id="company_cars_row_license_plate" max-length="16"/>
      |            <num db-column-name="employee_id" db-foreign-key="employees_row_id" id="company_cars_row_employee_id" max-digits="64"/>
      |            <num db-column-name="seats" id="company_cars_row_seats" max-digits="16"/>
      |            <date db-column-name="bought" id="company_cars_row_bought"/>
      |        </elem>
      |    </seq>
      |    <seq db-primary-key="id" id="employees">
      |        <elem id="employees_row">
      |            <num db-auto-inc="true" db-column-name="id" id="employees_row_id" max-digits="64"/>
      |            <str db-column-name="firstname" id="employees_row_firstname" max-length="64"/>
      |            <str db-column-name="lastname" id="employees_row_lastname" max-length="64"/>
      |            <date db-column-name="birthday" id="employees_row_birthday"/>
      |            <str db-column-name="notes" id="employees_row_notes"/>
      |            <num db-column-name="salary_group" db-foreign-key="salary_groups_row_id" id="employees_row_salary_group" max-digits="32"/>
      |        </elem>
      |    </seq>
      |    <seq db-primary-key="id" id="parking_slots">
      |        <elem id="parking_slots_row">
      |            <num db-auto-inc="true" db-column-name="id" id="parking_slots_row_id" max-digits="32"/>
      |            <num db-column-name="employee_id" db-foreign-key="employees_row_id" id="parking_slots_row_employee_id" max-digits="64"/>
      |            <str db-column-name="license_plate" id="parking_slots_row_license_plate" max-length="16"/>
      |        </elem>
      |    </seq>
      |    <seq db-primary-key="id" id="salary_groups">
      |        <elem id="salary_groups_row">
      |            <num db-auto-inc="true" db-column-name="id" id="salary_groups_row_id" max-digits="32"/>
      |            <str db-column-name="name" id="salary_groups_row_name" max-length="64"/>
      |            <formatnum db-column-name="min_wage" decimal-separator="." format="(-?\d{0,6}\.\d{0,2})" id="salary_groups_row_min_wage" max-digits="8" max-precision="2"/>
      |            <formatnum db-column-name="max_wage" decimal-separator="." format="(-?\d{0,6}\.\d{0,2})" id="salary_groups_row_max_wage" max-digits="8" max-precision="2"/>
      |        </elem>
      |    </seq>
      |    <seq db-primary-key="employee_id,salary_group" id="wages">
      |        <elem id="wages_row">
      |            <num db-column-name="employee_id" db-foreign-key="employees_row_id" id="wages_row_employee_id" max-digits="64"/>
      |            <num db-column-name="salary_group" db-foreign-key="salary_groups_row_id" id="wages_row_salary_group" max-digits="32"/>
      |            <formatnum db-column-name="wage" decimal-separator="." format="(-?\d{0,4}\.\d{0,2})" id="wages_row_wage" max-digits="6" max-precision="2"/>
      |        </elem>
      |    </seq>
      |</dfasdl>
    """.stripMargin

  "PostgreSQL database schema extraction" when {
    "using the generic mapper" when {
      "extract the correct schema" in {
        val connection = withClue("Database must have been created before the test!") {
          Try(
            DriverManager.getConnection(
              s"jdbc:postgresql://$databaseHost:$databasePort/$databaseName",
              databaseUser,
              databasePass
            )
          ) match {
            case scala.util.Failure(e) => fail(e.getMessage)
            case scala.util.Success(c) =>
              createDatabaseTables(c)
              c
          }
        }
        connection.close()
        val username: Option[JDBCUsername] = JDBCUsername.from(databaseUser).toOption
        val password: Option[JDBCPassword] =
          JDBCPassword.from(databasePass.getBytes(StandardCharsets.UTF_8)).toOption
        val jdbcUrl: JDBCUrl =
          JDBCUrl.unsafeFrom(s"jdbc:postgresql://$databaseHost:$databasePort/$databaseName")
        val result = DatabaseSchemaExtractor.extractSchema(password)(username)(jdbcUrl)
        result.unsafeRunSync() match {
          case Left(error) => fail(s"Extraction failed: $error")
          case Right(dfasdl) =>
            val checkXml = DiffBuilder
              .compare(Input.fromString(expectedDfasdlContent.trim))
              .withTest(dfasdl.toPrettyString)
              .ignoreWhitespace()
              .build()
            val diffs = xmlDiffDetails(checkXml)
            withClue(s"XML has differences: ${diffs.mkString("\n")}") {
              checkXml.hasDifferences() must be(false)
            }
        }
      }
    }

    "using the specific mapper" must {
      "extract the correct schema" in {
        val connection = withClue("Database must have been created before the test!") {
          Try(
            DriverManager.getConnection(
              s"jdbc:postgresql://$databaseHost:$databasePort/$databaseName",
              databaseUser,
              databasePass
            )
          ) match {
            case scala.util.Failure(e) =>
              fail(e.getMessage)
            case scala.util.Success(c) =>
              createDatabaseTables(c)
              c
          }
        }
        val jdbcUrl = connection.getMetaData().getURL()
        connection.close()
        DatabaseName.fromURI(new URI(jdbcUrl)) match {
          case Left(e) =>
            fail(e)
          case Right(dbName) =>
            val c = IO(
              DriverManager.getConnection(
                s"jdbc:postgresql://$databaseHost:$databasePort/$dbName",
                databaseUser,
                databasePass
              )
            )
            val extract = PostgreSQLSchemaExtractor.extractSchema(c)(dbName)
            extract.unsafeRunSync() match {
              case Left(error) => fail(s"Extraction failed: $error")
              case Right(dfasdl) =>
                val checkXml = DiffBuilder
                  .compare(Input.fromString(expectedDfasdlContent.trim))
                  .withTest(dfasdl.toPrettyString)
                  .ignoreWhitespace()
                  .build()
                val diffs = xmlDiffDetails(checkXml)
                withClue(s"XML has differences: ${diffs.mkString("\n")}") {
                  checkXml.hasDifferences() must be(false)
                }
            }
        }
      }
    }
  }
}
