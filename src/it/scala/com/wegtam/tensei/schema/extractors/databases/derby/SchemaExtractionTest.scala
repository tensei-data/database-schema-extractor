/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors.databases.derby

import java.net.URI
import java.nio.file._
import java.sql.{ Connection, DriverManager }

import cats.effect._
import cats.effect.unsafe.implicits.global
import com.wegtam.tensei.schema.extractors._
import com.wegtam.tensei.schema.extractors.IntegrationSpec
import com.wegtam.tensei.schema.extractors.PrettyPrinterInstances._
import com.wegtam.tensei.schema.extractors.PrettyPrinter.ops._
import com.wegtam.tensei.schema.extractors.databases._
import eu.timepit.refined.auto._
import com.wegtam.tensei.schema.extractors.databases.`package`.DatabaseName
import org.xmlunit.builder._

import scala.util.Try

class SchemaExtractionTest extends IntegrationSpec {

  // Create a temporary directory for the test database.
  private val databasePath = {
    val d = Files.createTempDirectory("derby-schema-extraction-test-")
    d.toAbsolutePath()
  }
  private val databaseName = "test"

  /** Disable security manager to avoid getting a
    * `java.security.AccessControlException` while testing.
    * Also we explicitly load the required JDBC driver because auto loading
    * somehow rarely works reliable.
    */
  override protected def beforeAll(): Unit = {
    System.setSecurityManager(null)
    val _ = Class.forName("org.apache.derby.iapi.jdbc.AutoloadedDriver")
  }

  /** Create the database before each test and fail if the directory
    * already exists.
    */
  override protected def beforeEach(): Unit = {
    val connection =
      DriverManager.getConnection(s"jdbc:derby:$databasePath/$databaseName;create=true")
    connection.close()
    super.beforeEach()
  }

  /** Delete the database directory after each test using a custom
    * file walker implementation.
    */
  override protected def afterEach(): Unit = {
    Try(DriverManager.getConnection(s"jdbc:derby:$databasePath/$databaseName;shutdown=true"))
    deleteDirectory(databasePath)
    super.afterEach()
  }

  /** Create the test tables within the database.
    *
    * @param connection A connection to the database.
    */
  private def createDatabaseTables(connection: Connection): Unit = {
    val s = connection.createStatement()
    s.execute("""
        |CREATE TABLE salary_groups (
        |  id INT GENERATED ALWAYS AS IDENTITY,
        |  name VARCHAR(64) NOT NULL,
        |  min_wage DECIMAL(8,2) NOT NULL,
        |  max_wage DECIMAL(8,2) NOT NULL,
        |  PRIMARY KEY (id),
        |  UNIQUE (name)
        |)
      """.stripMargin)
    s.execute("""
        |CREATE TABLE employees (
        |  id BIGINT GENERATED ALWAYS AS IDENTITY,
        |  firstname VARCHAR(64) NOT NULL,
        |  lastname VARCHAR(64) NOT NULL,
        |  birthday DATE NOT NULL,
        |  salary_group INT,
        |  PRIMARY KEY (id),
        |  FOREIGN KEY (salary_group) REFERENCES salary_groups (id)
        |)
      """.stripMargin)
    s.execute("""
        |CREATE TABLE wages (
        |  employee_id BIGINT,
        |  salary_group INT,
        |  wage DECIMAL(6,2) NOT NULL,
        |  PRIMARY KEY (employee_id, salary_group),
        |  FOREIGN KEY (employee_id) REFERENCES employees (id),
        |  FOREIGN KEY (salary_group) REFERENCES salary_groups (id)
        |)
      """.stripMargin)
    s.execute("""
        |CREATE TABLE company_cars (
        |  id INT GENERATED ALWAYS AS IDENTITY,
        |  license_plate VARCHAR(16) NOT NULL,
        |  employee_id BIGINT,
        |  seats SMALLINT,
        |  bought DATE,
        |  PRIMARY KEY (id),
        |  UNIQUE (license_plate),
        |  FOREIGN KEY (employee_id) REFERENCES employees (id)
        |)
      """.stripMargin)
    s.execute("""
        |CREATE TABLE parking_slots (
        |  id INT GENERATED ALWAYS AS IDENTITY,
        |  employee_id BIGINT,
        |  license_plate VARCHAR(16),
        |  PRIMARY KEY (id),
        |  FOREIGN KEY (employee_id) REFERENCES employees (id),
        |  UNIQUE (license_plate)
        |)
      """.stripMargin)
    s.close()
  }

  // Expected extraction result.
  val expectedDfasdlContentWithoutKeys =
    """
      |<?xml version="1.0" encoding="UTF-8" standalone="no"?>
      |<dfasdl xmlns="http://www.dfasdl.org/DFASDL" semantic="custom">
      |    <seq id="company_cars">
      |        <elem id="company_cars_row">
      |            <num db-auto-inc="true" db-column-name="id" id="company_cars_row_id"/>
      |            <str db-column-name="license_plate" id="company_cars_row_license_plate" max-length="16"/>
      |            <num db-column-name="employee_id" id="company_cars_row_employee_id"/>
      |            <num db-column-name="seats" id="company_cars_row_seats"/>
      |            <date db-column-name="bought" id="company_cars_row_bought"/>
      |        </elem>
      |    </seq>
      |    <seq id="employees">
      |        <elem id="employees_row">
      |            <num db-auto-inc="true" db-column-name="id" id="employees_row_id"/>
      |            <str db-column-name="firstname" id="employees_row_firstname" max-length="64"/>
      |            <str db-column-name="lastname" id="employees_row_lastname" max-length="64"/>
      |            <date db-column-name="birthday" id="employees_row_birthday"/>
      |            <num db-column-name="salary_group" id="employees_row_salary_group"/>
      |        </elem>
      |    </seq>
      |    <seq id="parking_slots">
      |        <elem id="parking_slots_row">
      |            <num db-auto-inc="true" db-column-name="id" id="parking_slots_row_id"/>
      |            <num db-column-name="employee_id" id="parking_slots_row_employee_id"/>
      |            <str db-column-name="license_plate" id="parking_slots_row_license_plate" max-length="16"/>
      |        </elem>
      |    </seq>
      |    <seq id="salary_groups">
      |        <elem id="salary_groups_row">
      |            <num db-auto-inc="true" db-column-name="id" id="salary_groups_row_id"/>
      |            <str db-column-name="name" id="salary_groups_row_name" max-length="64"/>
      |            <formatnum db-column-name="min_wage" decimal-separator="." format="(-?\d{0,6}\.\d{0,2})" id="salary_groups_row_min_wage" max-digits="8" max-precision="2"/>
      |            <formatnum db-column-name="max_wage" decimal-separator="." format="(-?\d{0,6}\.\d{0,2})" id="salary_groups_row_max_wage" max-digits="8" max-precision="2"/>
      |        </elem>
      |    </seq>
      |    <seq id="wages">
      |        <elem id="wages_row">
      |            <num db-column-name="employee_id" id="wages_row_employee_id"/>
      |            <num db-column-name="salary_group" id="wages_row_salary_group"/>
      |            <formatnum db-column-name="wage" decimal-separator="." format="(-?\d{0,4}\.\d{0,2})" id="wages_row_wage" max-digits="6" max-precision="2"/>
      |        </elem>
      |    </seq>
      |</dfasdl>
    """.stripMargin

  // Expected extraction result.
  val expectedDfasdlContentWithKeys =
    """
      |<?xml version="1.0" encoding="UTF-8" standalone="no"?>
      |<dfasdl xmlns="http://www.dfasdl.org/DFASDL" semantic="custom">
      |    <seq db-primary-key="id" id="company_cars">
      |        <elem id="company_cars_row">
      |            <num db-auto-inc="true" db-column-name="id" id="company_cars_row_id" max-digits="10"/>
      |            <str db-column-name="license_plate" id="company_cars_row_license_plate" max-length="16"/>
      |            <num db-column-name="employee_id" db-foreign-key="employees_row_id" id="company_cars_row_employee_id" max-digits="19"/>
      |            <num db-column-name="seats" id="company_cars_row_seats" max-digits="3"/>
      |            <date db-column-name="bought" id="company_cars_row_bought"/>
      |        </elem>
      |    </seq>
      |    <seq db-primary-key="id" id="employees">
      |        <elem id="employees_row">
      |            <num db-auto-inc="true" db-column-name="id" id="employees_row_id" max-digits="19"/>
      |            <str db-column-name="firstname" id="employees_row_firstname" max-length="64"/>
      |            <str db-column-name="lastname" id="employees_row_lastname" max-length="64"/>
      |            <date db-column-name="birthday" id="employees_row_birthday"/>
      |            <num db-column-name="salary_group" db-foreign-key="salary_groups_row_id" id="employees_row_salary_group" max-digits="10"/>
      |        </elem>
      |    </seq>
      |    <seq db-primary-key="id" id="parking_slots">
      |        <elem id="parking_slots_row">
      |            <num db-auto-inc="true" db-column-name="id" id="parking_slots_row_id" max-digits="10"/>
      |            <num db-column-name="employee_id" db-foreign-key="employees_row_id" id="parking_slots_row_employee_id" max-digits="19"/>
      |            <str db-column-name="license_plate" id="parking_slots_row_license_plate" max-length="16"/>
      |        </elem>
      |    </seq>
      |    <seq db-primary-key="id" id="salary_groups">
      |        <elem id="salary_groups_row">
      |            <num db-auto-inc="true" db-column-name="id" id="salary_groups_row_id" max-digits="10"/>
      |            <str db-column-name="name" id="salary_groups_row_name" max-length="64"/>
      |            <formatnum db-column-name="min_wage" decimal-separator="." format="(-?\d{0,6}\.\d{0,2})" id="salary_groups_row_min_wage" max-digits="8" max-precision="2"/>
      |            <formatnum db-column-name="max_wage" decimal-separator="." format="(-?\d{0,6}\.\d{0,2})" id="salary_groups_row_max_wage" max-digits="8" max-precision="2"/>
      |        </elem>
      |    </seq>
      |    <seq db-primary-key="employee_id,salary_group" id="wages">
      |        <elem id="wages_row">
      |            <num db-column-name="employee_id" db-foreign-key="employees_row_id" id="wages_row_employee_id" max-digits="19"/>
      |            <num db-column-name="salary_group" db-foreign-key="salary_groups_row_id" id="wages_row_salary_group" max-digits="10"/>
      |            <formatnum db-column-name="wage" decimal-separator="." format="(-?\d{0,4}\.\d{0,2})" id="wages_row_wage" max-digits="6" max-precision="2"/>
      |        </elem>
      |    </seq>
      |</dfasdl>
    """.stripMargin

  "Apache Derby database schema extraction" when {
    "using the generic mapper" when {
      "using a filesystem database" must {
        "extract the correct schema without keys" in {
          withClue("Database must have been created before the test!") {
            Try(DriverManager.getConnection(s"jdbc:derby:$databasePath/$databaseName")) match {
              case scala.util.Failure(e) => fail(e.getMessage())
              case scala.util.Success(c) =>
                createDatabaseTables(c)
                c.close()
            }
          }
          val username: Option[JDBCUsername] = None
          val password: Option[JDBCPassword] = None
          val jdbcUrl: JDBCUrl               = JDBCUrl.unsafeFrom(s"jdbc:derby:$databasePath/$databaseName")
          val result                         = DatabaseSchemaExtractor.extractSchema(password)(username)(jdbcUrl)
          result.unsafeRunSync() match {
            case Left(error) => fail(s"Extraction failed: $error")
            case Right(dfasdl) =>
              val checkXml = DiffBuilder
                .compare(Input.fromString(expectedDfasdlContentWithoutKeys.trim))
                .withTest(dfasdl.toPrettyString)
                .ignoreWhitespace()
                .build()
              val diffs = xmlDiffDetails(checkXml)
              withClue(s"XML has differences: ${diffs.mkString("\n")}") {
                checkXml.hasDifferences() must be(false)
              }
          }
        }
      }

      "using an in-memory database" must {
        "extract the correct schema without keys" ignore {
          fail(
            "The generic extractor cannot re-use existing in memory databases because they are bound to an existing connection!"
          )
        }
      }
    }

    "using a filesystem database" must {
      "extract the correct schema without keys" in {
        val connection =
          withClue("Database must have been created before the test!") {
            Try(DriverManager.getConnection(s"jdbc:derby:$databasePath/$databaseName")) match {
              case scala.util.Failure(e) => fail(e.getMessage())
              case scala.util.Success(c) =>
                createDatabaseTables(c)
                c
            }
          }
        DatabaseName.fromURI(new URI(connection.getMetaData().getURL())) match {
          case Left(e) =>
            connection.close()
            fail(e)
          case Right(dbName) =>
            connection.close()
            val c       = IO(DriverManager.getConnection(s"jdbc:derby:$databasePath/$databaseName"))
            val extract = DerbySchemaExtractor.extractSchema(c)(dbName)
            extract.unsafeRunSync() match {
              case Left(error) => fail(s"Extraction failed: $error")
              case Right(dfasdl) =>
                val checkXml = DiffBuilder
                  .compare(Input.fromString(expectedDfasdlContentWithoutKeys.trim))
                  .withTest(dfasdl.toPrettyString)
                  .ignoreWhitespace()
                  .build()
                val diffs = xmlDiffDetails(checkXml)
                withClue(s"XML has differences: ${diffs.mkString("\n")}") {
                  checkXml.hasDifferences() must be(false)
                }
            }
        }
      }
    }

    "using an in-memory database" must {
      "extract the correct schema without keys" in {
        val connection =
          withClue("Database must have been created before the test!") {
            Try(DriverManager.getConnection(s"jdbc:derby:memory:$databaseName;create=true")) match {
              case scala.util.Failure(e) => fail(e.getMessage())
              case scala.util.Success(c) =>
                createDatabaseTables(c)
                c
            }
          }
        // Close the in-memory database _after_ extraction to avoid data loss!
        DatabaseName.fromURI(new URI(connection.getMetaData().getURL())) match {
          case Left(e) =>
            connection.close()
            fail(e)
          case Right(dbName) =>
            val c       = IO(DriverManager.getConnection(s"jdbc:derby:memory:$databaseName"))
            val extract = DerbySchemaExtractor.extractSchema(c)(dbName)
            extract.unsafeRunSync() match {
              case Left(error) =>
                connection.close()
                fail(s"Extraction failed: $error")
              case Right(dfasdl) =>
                connection.close()
                val checkXml = DiffBuilder
                  .compare(Input.fromString(expectedDfasdlContentWithoutKeys.trim))
                  .withTest(dfasdl.toPrettyString)
                  .ignoreWhitespace()
                  .build()
                val diffs = xmlDiffDetails(checkXml)
                withClue(s"XML has differences: ${diffs.mkString("\n")}") {
                  checkXml.hasDifferences() must be(false)
                }
            }
        }
      }
    }
  }
}
