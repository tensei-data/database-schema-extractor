/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors.databases.sqlite

import java.util.Locale

import cats.syntax.option._
import com.wegtam.tensei.schema.extractors.databases._
import com.wegtam.tensei.schema.extractors.databases.DatabaseColumnType._
import eu.timepit.refined.auto._

@SuppressWarnings(Array("org.wartremover.warts.StringPlusAny"))
object DatabaseColumnTypeUtilsInstances {
  // TODO Map `UUID` and `JSON` to something useable.

  implicit val sqliteDatabaseColumnTypeUtils: DatabaseColumnTypeUtils[DatabaseColumnTypeName] =
    new DatabaseColumnTypeUtils[DatabaseColumnTypeName] {
      override def toType(a: DatabaseColumnTypeName): Option[DatabaseColumnType] =
        a.toUpperCase(Locale.ROOT).takeWhile(_.isUpper) match {
          case "BLOB"                                                      => Binary.some
          case "DATE"                                                      => Date.some
          case "DATETIME"                                                  => DateTime.some
          case "REAL" | "DOUBLE" | "DOUBLE PRECISION" | "FLOAT" | "NUMBER" => FormatNum.some
          case "INT" | "INTEGER" | "TINYINT" | "SMALLINT" | "MEDIUMINT" | "BIGINT" | "UNSIGNED BIG INT" | "INT2" |
              "INT8" =>
            Num.some
          case "CHARACTER" | "CLOB" | "NATIVE CHARACTER" | "NCHAR" | "NVARCHAR" | "VARYING CHARACTER" | "VARCHAR" =>
            Str.some
          case _ => Str.some // Currently we map everything not specified to a `STR` element!
        }
    }

}
