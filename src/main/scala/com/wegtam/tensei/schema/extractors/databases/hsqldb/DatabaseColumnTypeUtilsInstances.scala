/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors.databases.hsqldb

import java.util.Locale

import cats.syntax.option._
import com.wegtam.tensei.schema.extractors.databases._
import com.wegtam.tensei.schema.extractors.databases.DatabaseColumnType._
import eu.timepit.refined.auto._

@SuppressWarnings(Array("org.wartremover.warts.StringPlusAny"))
object DatabaseColumnTypeUtilsInstances {
  // TODO Map `UUID` and `JSON` to something useable.

  implicit val hsqlDatabaseColumnTypeUtils: DatabaseColumnTypeUtils[DatabaseColumnTypeName] =
    new DatabaseColumnTypeUtils[DatabaseColumnTypeName] {
      override def toType(a: DatabaseColumnTypeName): Option[DatabaseColumnType] =
        a.toUpperCase(Locale.ROOT).takeWhile(_.isUpper) match {
          case "BINARY" | "BINARY LARGE OBJECT" | "BINARY VARYING" | "BIT" | "BIT VARYING" | "BLOB" | "LONGVARBINARY" |
              "VARBINARY" =>
            Binary.some
          case "DATE"                                                                   => Date.some
          case "TIMESTAMP" | "TIMESTAMP WITH TIME ZONE" | "TIMESTAMP WITHOUT TIME ZONE" => DateTime.some
          case "DOUBLE" | "DECIMAL" | "FLOAT" | "NUMERIC" | "REAL"                      => FormatNum.some
          case "BIGINT" | "INTEGER" | "SMALLINT" | "TINYINT"                            => Num.some
          case "CHAR" | "CHARACTER" | "CHARACTER LARGE OBJECT" | "CHARACTER VARYING" | "CLOB" | "LONGVARCHAR" |
              "VARCHAR" =>
            Str.some
          case "TIME" | "TIME WITH TIME ZONE" | "TIME WITHOUT TIME ZONE" => Time.some
          case _                                                         => Str.some // Currently we map everything not specified to a `STR` element!
        }
    }

}
