/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors.databases

import cats.implicits._

/** A database table description.
  *
  * @param name       The name of the table in the database.
  * @param primaryKey A list of column names that form the primary key of the table.
  */
final case class DatabaseTable(name: DatabaseTableName, primaryKey: List[DatabaseColumnName])

object DatabaseTable {

  /** Create a database table description from unsafe input.
    *
    * @param name       A string holding the name of the table.
    * @param primaryKey A list of column names which describe the primary key.
    * @return A option to the matching database table description.
    */
  def from(name: String, primaryKey: List[String]): Option[DatabaseTable] =
    DatabaseTableName.from(name).toOption.flatMap { tn =>
      val pk = primaryKey.flatMap(c => DatabaseColumnName.from(c).toOption)
      if (pk.lengthCompare(primaryKey.length) === 0) {
        DatabaseTable(
          name = tn,
          primaryKey = pk
        ).some
      } else
        None
    }
}
