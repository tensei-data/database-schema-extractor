/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors

import java.net.URI

import cats.implicits._
import eu.timepit.refined._
import eu.timepit.refined.api._
import eu.timepit.refined.auto._
import eu.timepit.refined.cats._
import eu.timepit.refined.collection._
import eu.timepit.refined.string._

import scala.util.Try

package object databases {

  type DatabaseColumnName = String Refined NonEmpty
  object DatabaseColumnName extends RefinedTypeOps[DatabaseColumnName, String] with CatsRefinedTypeOpsSyntax

  type DatabaseColumnTypeName = String Refined NonEmpty
  object DatabaseColumnTypeName extends RefinedTypeOps[DatabaseColumnTypeName, String] with CatsRefinedTypeOpsSyntax

  type DatabaseName = String Refined NonEmpty
  object DatabaseName extends RefinedTypeOps[DatabaseName, String] with CatsRefinedTypeOpsSyntax {

    /** Try to extract the database name from a given JDBC URI.
      *
      * <p>If the database name contains forward slashes (`/`) then it is
      * split up and the part after the last slash is returned.</p>
      *
      * @param u An URI holding a valid JDBC connection description.
      * @return Either the extracted database name or an error.
      */
    @SuppressWarnings(Array("org.wartremover.warts.Null"))
    def fromURI(u: URI): Either[String, DatabaseName] =
      // TODO Refactor this and maybe pull it out into a typeclass.
      u.getSchemeSpecificPart() match {
        case "" =>
          "Could not extract scheme specific part from URI!".asLeft[DatabaseName]
        case sp =>
          Try {
            val du = new URI(sp)
            Option(du.getScheme()).flatMap { driver =>
              driver match {
                case "inetdae" =>
                  du.getSchemeSpecificPart()
                    .split("\\?")
                    .filter(_.startsWith("database"))
                    .headOption
                    .flatMap(_.split("=").takeRight(1).headOption)
                case "sqlserver" =>
                  du.getSchemeSpecificPart
                    .split(";")
                    .filter(_.startsWith("databaseName"))
                    .headOption
                    .flatMap(_.split("=").takeRight(1).headOption)
                case "rmi" =>
                  Option(new URI(du.getPath().substring(1)).getScheme()).map(s =>
                    s.split("\\/").lastOption.getOrElse(s)
                  )
                case _ =>
                  val path = du.getPath() match {
                    case null => du.getSchemeSpecificPart().split(":").takeRight(1).toList
                    case path => List(path)
                  }
                  path.headOption.map { name =>
                    val part =
                      if (name.startsWith("/") || name.startsWith("@"))
                        name.substring(1).split("\\/").lastOption.getOrElse(name.substring(1))
                      else
                        name.split("\\/").lastOption.getOrElse(name)
                    part.takeWhile(c => (c =!= ';' && c =!= '?')) // Omit possibly appended parameters
                  }
              }
            }
          } match {
            case scala.util.Failure(_) =>
              "An exception was thrown while trying to extract the database name from the URI!"
                .asLeft[DatabaseName]
            case scala.util.Success(n) =>
              n match {
                case None       => "Could not extract database name from URI!".asLeft[DatabaseName]
                case Some(name) => DatabaseName.from(name)
              }
          }
      }
  }

  type DatabaseTableName = String Refined NonEmpty
  object DatabaseTableName extends RefinedTypeOps[DatabaseTableName, String] with CatsRefinedTypeOpsSyntax

  type DatabaseQueryString = String Refined NonEmpty
  object DatabaseQueryString extends RefinedTypeOps[DatabaseQueryString, String] with CatsRefinedTypeOpsSyntax

  type ErrorMessage = String Refined NonEmpty
  object ErrorMessage extends RefinedTypeOps[ErrorMessage, String] with CatsRefinedTypeOpsSyntax

  type ForeignKey = String Refined NonEmpty
  object ForeignKey extends RefinedTypeOps[ForeignKey, String] with CatsRefinedTypeOpsSyntax

  type JDBCUrl = String Refined MatchesRegex[W.`"^jdbc:[a-zA-z0-9]+:.*"`.T]
  object JDBCUrl extends RefinedTypeOps[JDBCUrl, String] with CatsRefinedTypeOpsSyntax

  type JDBCUsername = String Refined NonEmpty
  object JDBCUsername extends RefinedTypeOps[JDBCUsername, String] with CatsRefinedTypeOpsSyntax

  type JDBCPassword = Array[Byte] Refined NonEmpty
  object JDBCPassword extends RefinedTypeOps[JDBCPassword, Array[Byte]] with CatsRefinedTypeOpsSyntax
}
