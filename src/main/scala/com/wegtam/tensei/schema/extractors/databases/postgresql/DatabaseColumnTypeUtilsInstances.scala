/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors.databases.postgresql

import java.util.Locale

import cats.syntax.option._
import com.wegtam.tensei.schema.extractors.databases._
import com.wegtam.tensei.schema.extractors.databases.DatabaseColumnType._
import eu.timepit.refined.auto._

@SuppressWarnings(Array("org.wartremover.warts.StringPlusAny"))
object DatabaseColumnTypeUtilsInstances {

  implicit val postgresqlDatabaseColumnTypeUtils: DatabaseColumnTypeUtils[DatabaseColumnTypeName] =
    new DatabaseColumnTypeUtils[DatabaseColumnTypeName] {
      override def toType(a: DatabaseColumnTypeName): Option[DatabaseColumnType] =
        a.toUpperCase(Locale.ROOT).takeWhile(_.isUpper) match {
          case "BYTEA"                                                    => Binary.some
          case "DATE"                                                     => Date.some
          case "TIMESTAMP WITH TIME ZONE" | "TIMESTAMP WITHOUT TIME ZONE" => DateTime.some
          case "DOUBLE PRECISION" | "NUMERIC"                             => FormatNum.some
          case "BIGINT" | "BIGSERIAL" | "INT" | "INTEGER" | "SERIAL" | "SMALLINT" | "SMALLSERIAL" =>
            Num.some
          case "CHAR" | "CHARACTER" | "CHARACTER VARYING" | "TEXT" | "VARCHAR" => Str.some
          case "TIME WITH TIME ZONE" | "TIME WITHOUT TIME ZONE"                => Time.some
          case _                                                               => Str.some // Currently we map everything not specified to a `STR` element!
        }
    }

}
