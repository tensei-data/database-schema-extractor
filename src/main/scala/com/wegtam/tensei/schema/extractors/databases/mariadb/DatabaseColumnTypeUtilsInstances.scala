/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors.databases.mariadb

import java.util.Locale

import cats.syntax.option._
import com.wegtam.tensei.schema.extractors.databases._
import com.wegtam.tensei.schema.extractors.databases.DatabaseColumnType._
import eu.timepit.refined.auto._

@SuppressWarnings(Array("org.wartremover.warts.StringPlusAny"))
object DatabaseColumnTypeUtilsInstances {

  implicit val mariadbDatabaseColumnTypeUtils: DatabaseColumnTypeUtils[DatabaseColumnTypeName] =
    new DatabaseColumnTypeUtils[DatabaseColumnTypeName] {
      override def toType(a: DatabaseColumnTypeName): Option[DatabaseColumnType] =
        a.toUpperCase(Locale.ROOT).takeWhile(_.isUpper) match {
          case "BINARY" | "BLOB" | "CHAR BYTE" | "LONGBLOB" | "MEDIUMBLOB" | "TINYBLOB" | "VARBINARY" =>
            Binary.some
          case "DATE"                   => Date.some
          case "DATETIME" | "TIMESTAMP" => DateTime.some
          case "DEC" | "DECIMAL" | "DOUBLE" | "DOUBLE PRECISION" | "FIXED" | "FLOAT" | "NUMERIC" =>
            FormatNum.some
          case "BIGINT" | "BIT" | "BOOLEAN" | "INT" | "INTEGER" | "MEDIUMINT" | "SMALLINT" | "TINYINT" =>
            Num.some
          case "CHAR" | "CHARACTER" | "CHAR VARYING" | "CHARACTER VARYING" | "LONG VARCHAR" | "VARCHAR" =>
            Str.some
          case "TIME" => Time.some
          case _      => Str.some // Currently we map everything not specified to a `STR` element!
        }
    }

}
