/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors

import java.nio.charset.StandardCharsets
import java.sql.DriverManager

import cats.effect._
import cats.implicits._
import com.monovore.decline._
import com.monovore.decline.effect._
import com.monovore.decline.refined._
import com.wegtam.tensei.schema.extractors.PrettyPrinterInstances._
import com.wegtam.tensei.schema.extractors.PrettyPrinter.ops._
import com.wegtam.tensei.schema.extractors.databases._
import eu.timepit.refined.auto._
import eu.timepit.refined.cats._
import eu.timepit.refined.types.string.NonEmptyString

import scala.collection.mutable

object CLIConstants {
  final val name: ApplicationName       = "database-schema-extractor"
  final val header: ApplicationHeader   = "Tensei database schema extractor."
  final val version: ApplicationVersion = "0.1.0"
}

object CLI
    extends CommandIOApp(
      name = CLIConstants.name,
      header = CLIConstants.header,
      version = CLIConstants.version
    ) {
  val user: Opts[JDBCUsername] = Opts.option[JDBCUsername](
    long = "login",
    help = "The login i.e. username for the database connection.",
    short = "l"
  )
  val pass: Opts[NonEmptyString] = Opts.option[NonEmptyString](
    long = "password",
    help = "The password for the database connection.",
    short = "p"
  )
  val url: Opts[JDBCUrl] = Opts.option[JDBCUrl](
    long = "url",
    help = "A valid JDBC url for the database connection.",
    short = "u"
  )

  /** Return a list of all driver names registered with the JDBC
    * DriverManager.
    * <p><strong>This can be used as a workaround for the dreaded
    * `No suitable driver found for ...` exception.</strong></p>
    *
    * @return A list of class names which might be empty.
    */
  private def fetchDrivers(): IO[List[String]] =
    IO(DriverManager.getDrivers()).map { it =>
      val ds = mutable.ListBuffer.empty[String]
      while (it.hasMoreElements()) {
        val _ = ds += it.nextElement().getClass().getCanonicalName()
      }
      ds.toList
    }

  @SuppressWarnings(Array("org.wartremover.warts.Null"))
  override def main: Opts[IO[ExitCode]] = {
    // TODO Find another workaround to get rid of `java.security.AccessControlException` when trying to load a driver.
    System.setSecurityManager(null)
    val ou = user.orNone
    val op =
      pass.orNone.map(
        _.flatMap(p => JDBCPassword.from(p.getBytes(StandardCharsets.UTF_8)).toOption)
      )
    (url, ou, op).mapN { case (jdbcUrl, username, password) =>
      val program = for {
        _ <- fetchDrivers()
        code <- DatabaseSchemaExtractor.extractSchema(password)(username)(jdbcUrl).map {
          case Left(error) =>
            println(s"An error occured: ${error.show}")
            ExitCode.Error
          case Right(dfasdl) =>
            println(dfasdl.toPrettyString)
            ExitCode.Success
        }
      } yield code
      program.attempt.map {
        case Left(exception) =>
          println(s"An exception was thrown: ${exception.getMessage()}")
          exception.printStackTrace()
          ExitCode.Error
        case Right(code) => code
      }
    }
  }
}
