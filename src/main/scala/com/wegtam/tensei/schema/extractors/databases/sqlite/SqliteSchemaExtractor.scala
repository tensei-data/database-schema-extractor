/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors.databases.sqlite

import java.sql.{ Connection, PreparedStatement }
import java.util.Locale

import cats.data.NonEmptyList
import cats.effect._
import cats.implicits._
import com.wegtam.tensei.schema.extractors._
import com.wegtam.tensei.schema.extractors.databases._
import com.wegtam.tensei.schema.extractors.databases.DatabaseColumnFlags._
import com.wegtam.tensei.schema.extractors.databases.sqlite.DatabaseColumnTypeUtilsInstances._
import com.wegtam.tensei.schema.extractors.databases.DatabaseColumnTypeUtils.ops._
import eu.timepit.refined.auto._
import eu.timepit.refined.cats._
import eu.timepit.refined.types.numeric.NonNegLong
import org.dfasdl._

import scala.collection.mutable

/** A buffer for the raw metadata of a table column extracted from the
  * query.
  *
  * @param columnName    The name of the column.
  * @param columnType    The type description of the column.
  * @param nullable      A flag indicating if the column is allowed to be null.
  * @param precision     The precision (length / size) of the column.
  * @param scale         The scale of the column (will mostly be zero).
  * @param autoIncrement A flag indicating if the column is an auto increment column.
  * @param foreignKey    The foreign key definition of the column.
  * @param isPrimaryKey  A flag indicating if the column belongs to the primary key.
  * @param defaultValue  The default value of the column.
  */
final case class RawColumnMetadata(
    columnName: String,
    columnType: String,
    nullable: Boolean,
    precision: Option[Long],
    scale: Option[Long],
    autoIncrement: Boolean,
    foreignKey: Option[String],
    isPrimaryKey: Boolean,
    default: Option[String]
)

object SqliteSchemaExtractor {

  /** Extract a schema description from a given database connection and
    * return it in the format of the Data Format and Semantics Description
    * Language (DFASDL).
    *
    * @param c      A database connection.
    * @param dbName The name of the database.
    * @return Either a DFASDL XML document or an error message.
    */
  def extractSchema(
      c: IO[Connection]
  )(dbName: DatabaseName): IO[Either[ErrorMessage, org.w3c.dom.Document]] = {
    val listTables   = prepareListTablesStatement(c)
    val tableDetails = prepareDetailStatements()
    val document = for {
      tables <- getTables(listTables)(dbName)
      dfasdl = DFASDLFunctions.initDocument
      tablesData <- tables.traverse(t => describeTable(dfasdl)(c)(tableDetails)(t))
      _ = tablesData.foreach(_.foreach(t => dfasdl.getDocumentElement().appendChild(t)))
    } yield dfasdl.asRight[ErrorMessage]
    document
  }

  /** Create a specific DFASDL Data-Element that represents a column of an actual table. Depending
    * on the type of the column, the Data-Element is created with the appropriate tag name and
    * additional attributes.
    *
    * @param d          The XML document.
    * @param tableName  The name of the table.
    * @param flags      A list of flags for the column (auto increment, nullable) which might be empty.
    * @param fk         The foreign key definition for the column.
    * @param default    An optional string containing the default value for the column.
    * @param scale      An optional scale of the column (e.g. 2,34 means scale 2). It will be 0 in most cases.
    * @param precision  An optional precision aka "length" of the column.
    * @param columnType The type of the column in the database table.
    * @param columnName The name of the column in the database table.
    * @return An element that represents the column.
    */
  protected def createColumnElement(d: org.w3c.dom.Document)(tableName: DatabaseTableName)(
      flags: List[DatabaseColumnFlags]
  )(fk: Option[ForeignKey])(default: Option[String])(scale: Option[NonNegLong])(
      precision: Option[NonNegLong]
  )(
      columnType: DatabaseColumnTypeName
  )(columnName: DatabaseColumnName): Option[org.w3c.dom.Element] = {
    val element = columnType.toType.map { t =>
      t match {
        case DatabaseColumnType.Binary =>
          // FIXME Actually use the binary element here!
          val e = d.createElement(Element.STRING.tagName)
          precision.foreach(p => e.setAttribute(Attribute.MAX_LENGTH.name, p.show))
          default.foreach(d => e.setAttribute(Attribute.DEFAULT_STRING.name, d.show))
          e
        case DatabaseColumnType.Date =>
          val e = d.createElement(Element.DATE.tagName)
          e
        case DatabaseColumnType.DateTime =>
          val e = d.createElement(Element.DATETIME.tagName)
          e
        case DatabaseColumnType.FormatNum =>
          val e = d.createElement(Element.FORMATTED_NUMBER.tagName)
          default.foreach(d => e.setAttribute(Attribute.DEFAULT_NUMBER.name, d.show))
          scale.foreach { s =>
            e.setAttribute(Attribute.MAX_PRECISION.name, s.show)
            e.setAttribute(Attribute.DECIMAL_SEPARATOR.name, ".")
          }
          precision.foreach { p =>
            e.setAttribute(Attribute.MAX_DIGITS.name, p.show)
          }
          DFASDLFunctions
            .createFormatnumRegex(DFASDLFunctions.DEFAULT_NUMERIC_SEPARATOR)(precision)(scale)
            .foreach(r => e.setAttribute(Attribute.FORMAT.name, r.show))
          e
        case DatabaseColumnType.Num =>
          val e = d.createElement(Element.NUMBER.tagName)
          default.foreach(d => e.setAttribute(Attribute.DEFAULT_NUMBER.name, d.show))
          scale.foreach(s => e.setAttribute(Attribute.PRECISION.name, s.show))
          precision.foreach(p => e.setAttribute(Attribute.MAX_DIGITS.name, p.show))
          e
        case DatabaseColumnType.Str =>
          val e = d.createElement(Element.STRING.tagName)
          precision.foreach(p => e.setAttribute(Attribute.MAX_LENGTH.name, p.show))
          default.foreach(d => e.setAttribute(Attribute.DEFAULT_STRING.name, d))
          e
        case DatabaseColumnType.Time =>
          val e = d.createElement(Element.TIME.tagName)
          e
      }
    }
    element.map { e =>
      e.setAttribute(DFASDLFunctions.ID_ATTR, s"${tableName.show}_row_${columnName.show}")
      e.setAttribute(Attribute.DB_COLUMN_NAME.name, columnName.show)
      flags
        .find(_ === DatabaseColumnFlags.AutoIncrement)
        .foreach(_ => e.setAttribute(Attribute.DB_AUTO_INCREMENT.name, "true"))
      fk.foreach(cs => e.setAttribute(Attribute.DB_FOREIGN_KEY.name, cs.show.toLowerCase(Locale.ROOT)))
      e
    }
  }

  /** Return a list of tables using the given prepared statement and
    * database name.
    *
    * @param s A prepared statement to list all tables.
    * @param n The name of the database.
    * @return A list of database table description which can be used to extract more details from the schema.
    */
  @SuppressWarnings(Array("org.wartremover.warts.Null"))
  protected def getTables(s: IO[PreparedStatement])(n: DatabaseName): IO[List[DatabaseTable]] =
    s.map { stmt =>
      val ts = mutable.ListBuffer.empty[DatabaseTable]
      if (stmt.getParameterMetaData().getParameterCount() > 0) {
        stmt.setString(1, n)
      }
      val rs   = stmt.executeQuery()
      val meta = rs.getMetaData()
      while (rs.next()) {
        val tn = rs.getString("TABLE_NAME").toLowerCase(Locale.ROOT)
        val pk =
          if (meta.getColumnCount() > 1 && rs.getString("PK_COLUMNS") =!= null)
            rs.getString("PK_COLUMNS").toLowerCase(Locale.ROOT).split(",").toList
          else
            List.empty
        DatabaseTable.from(tn, pk).foreach(t => ts += t)
      }
      ts.toList
    }

  /** Create a description of a database table within an Sqlite database.
    *
    * @param d          A DFASDL document that will be used for the table description.
    * @param c          A database connection.
    * @param statements A list SQL statements that are used to extract necessary information from the database.
    * @param t          The table description model holding the name and other necessary information.
    * @return An option to a DFASDL element describing the database table.
    */
  protected def describeTable(d: org.w3c.dom.Document)(
      c: IO[Connection]
  )(statements: NonEmptyList[DatabaseQueryString])(t: DatabaseTable): IO[Option[org.w3c.dom.Element]] =
    if (t.name.equalsIgnoreCase("sqlite_sequence")) {
      IO.pure(none[org.w3c.dom.Element])
    } else {
      c.map { con =>
        val metadata = {
          val lengthPrecisionRegEx = "^\\((\\d+),?(\\d+)?\\).*".r
          // Due to the nature of having to use `while` we need a buffer to fill.
          val rawMetadata = mutable.ListBuffer.empty[RawColumnMetadata]
          // Get foreign key information.
          val fkColumns =
            try {
              val fkStmt = con.createStatement()
              val fkData = fkStmt.executeQuery(statements.head.replace("?", t.name))
              val tmpMap = scala.collection.mutable.Map.empty[String, String]
              while (fkData.next()) {
                val _ = tmpMap += (fkData.getString("from") -> s"""${fkData.getString("table")}_row_${fkData.getString(
                  "to"
                )}""")
              }
              fkStmt.close()
              tmpMap.toMap
            } catch {
              case _: java.sql.SQLException =>
                // SQLite throws an SQL exception while trying to find foreign key columns instead of returning an empty ResultSet.
                Map.empty[String, String]
            }
          // Get columns with unique constraints.
          //val uniqueColumns =
          //  try {
          //    val fallbackQuery: DatabaseQueryString = """PRAGMA index_list(?)"""
          //    val indexStmt                          = con.createStatement()
          //    val indexInfo =
          //      indexStmt.executeQuery(statements.tail.headOption.getOrElse(fallbackQuery).replace("?", t.name))
          //    val uis = mutable.ListBuffer.empty[String]
          //    while (indexInfo.next())
          //      if (indexInfo.getInt("unique") === 1 && indexInfo.getString("origin") =!= "pk")
          //        uis += indexInfo.getString("name")
          //    val unique = uis.toList.flatMap { column =>
          //      val fallbackQuery: DatabaseQueryString = """PRAGMA index_info(?)"""
          //      val i = indexStmt.executeQuery(
          //        statements.tail.drop(1).headOption.getOrElse(fallbackQuery).replace("?", column)
          //      )
          //      if (i.next())
          //        i.getString("name").some
          //      else
          //        none[String]
          //    }
          //    indexInfo.close()
          //    unique
          //  } catch {
          //    case e: java.sql.SQLException =>
          //      List.empty[String]
          //  }
          // Get primary key columns beforehand because they are needed to figure out auto increment columns later.
          val tableInfoStmt = con.createStatement()
          val primaryKeyColumns =
            try {
              val pkInfo = tableInfoStmt.executeQuery(statements.last.replace("?", t.name))
              val pks    = mutable.Queue.empty[String]
              while (pkInfo.next()) {
                val _ =
                  if (pkInfo.getInt("pk") > 0)
                    pks.append(pkInfo.getString("name"))
              }
              pks.toList
            } catch {
              case _: java.sql.SQLException =>
                List.empty[String]
            }
          // Get base table information.
          val tableInfo = tableInfoStmt.executeQuery(statements.last.replace("?", t.name))
          while (tableInfo.next()) {
            val columnName = tableInfo.getString("name")
            val (columnType, precision, scale) = if (tableInfo.getString("type").contains("(")) {
              val t = tableInfo.getString("type").dropWhile(_ != '(')
              lengthPrecisionRegEx.findFirstMatchIn(t) match {
                case None => (tableInfo.getString("type"), none[Long], none[Long])
                case Some(m) =>
                  m.groupCount match {
                    case 0 => (tableInfo.getString("type"), none[Long], none[Long])
                    case _ =>
                      (tableInfo.getString("type"), Option(m.group(1)).map(_.toLong), Option(m.group(2)).map(_.toLong))
                  }
              }
            } else {
              (tableInfo.getString("type"), none[Long], none[Long])
            }
            val autoIncrement = columnType.equalsIgnoreCase("INTEGER") || columnType.equalsIgnoreCase(
              "INT"
            ) && primaryKeyColumns.contains(columnName) && primaryKeyColumns.size === 1
            val notNull = tableInfo.getBoolean("notnull")
            val default =
              if (Option(tableInfo.getString("dflt_value")).map(_.nonEmpty).getOrElse(false))
                Option(tableInfo.getString("dflt_value"))
              else
                None
            rawMetadata += RawColumnMetadata(
              columnName = columnName,
              columnType = columnType,
              nullable = !notNull,
              precision = precision,
              scale = scale,
              autoIncrement = autoIncrement,
              foreignKey = fkColumns.get(columnName),
              isPrimaryKey = primaryKeyColumns.contains(columnName),
              default = default
            )
          }
          tableInfoStmt.close()
          rawMetadata.toList
        }
        val ot = for {
          table <- DFASDLFunctions.createTableSkeletonElement(d)(t)
          rows  <- DFASDLFunctions.getFirstNode(table.getElementsByTagName(Element.ELEMENT.tagName))
          pks = mutable.ListBuffer.empty[String]
          _ = metadata.map { m =>
            (
              DatabaseColumnName.from(m.columnName.toLowerCase(Locale.ROOT)),
              DatabaseColumnTypeName.from(m.columnType)
            ).mapN { case (columnName, columnType) =>
              val precision = m.precision.flatMap(p => NonNegLong.from(p).toOption)
              val scale     = m.scale.flatMap(s => NonNegLong.from(s).toOption).filterNot(_ <= 0)
              val nullable =
                if (m.nullable)
                  List(Nullable)
                else
                  Nil
              val autoIncrement =
                if (m.autoIncrement)
                  List(AutoIncrement)
                else
                  Nil
              val flags   = nullable ::: autoIncrement
              val fk      = m.foreignKey.flatMap(s => ForeignKey.from(s).toOption)
              val default = m.default
              val _ =
                if (m.isPrimaryKey)
                  pks.append(m.columnName)
              val column = createColumnElement(d)(t.name)(flags)(fk)(default)(scale)(precision)(
                columnType
              )(columnName)
              column.foreach(rows.appendChild)
            }
          }
        } yield (table, pks.toList)

        ot.map { t =>
          val (tableElement, primaryKeyColumns) = t
          if (primaryKeyColumns.nonEmpty)
            tableElement.setAttribute(Attribute.DB_PRIMARY_KEY.name, primaryKeyColumns.mkString(","))
          tableElement
        }
      }
    }

  /** Create a list of statements that are used to fetch table details from the
    * database. In most cases the returned list will only include a single
    * statement. If more statements are returned the appropriate function that
    * uses them must know how to do this. Usually they are* executed in the
    * defined order (0 -> 1 -> 2).
    *
    * Because SQLite doesn't support prepared statements for PRAGMA queries we
    * return the raw SQL strings here!
    *
    * @return A list of SQL statements.
    */
  protected def prepareDetailStatements(): NonEmptyList[DatabaseQueryString] = {
    val queries: NonEmptyList[DatabaseQueryString] = NonEmptyList.of(
      """PRAGMA foreign_key_list(?)""",
      """PRAGMA index_list(?)""",
      """PRAGMA index_info(?)""",
      """PRAGMA table_info(?)"""
    )
    queries
  }

  /** Prepare a statement that lists all user tables from the database.
    *
    * @param c A database connection.
    * @return A prepared statement which will return a list of all table names in the database.
    */
  protected def prepareListTablesStatement(c: IO[Connection]): IO[PreparedStatement] = {
    val query: DatabaseQueryString =
      """SELECT name AS TABLE_NAME FROM sqlite_master WHERE UPPER(type) = 'TABLE' ORDER BY name"""
    c.map(_.prepareStatement(query))
  }
}
