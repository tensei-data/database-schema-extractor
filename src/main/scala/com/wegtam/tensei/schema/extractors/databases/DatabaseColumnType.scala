/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors.databases

import enumeratum._

/** A sealed trait for our supported database column types. Their instances
  * will be mapped to the corresponding DFASDL element types.
  */
sealed trait DatabaseColumnType extends EnumEntry with Product with Serializable

object DatabaseColumnType extends Enum[DatabaseColumnType] {
  val values = findValues

  case object Binary    extends DatabaseColumnType
  case object Date      extends DatabaseColumnType
  case object DateTime  extends DatabaseColumnType
  case object FormatNum extends DatabaseColumnType
  case object Num       extends DatabaseColumnType
  case object Str       extends DatabaseColumnType
  case object Time      extends DatabaseColumnType
}
