/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors.databases

import cats.implicits._
import eu.timepit.refined.auto._
import eu.timepit.refined.cats._
import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.matchers.must.Matchers
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class DatabaseTableTest extends AnyWordSpec with Matchers with ScalaCheckPropertyChecks {

  "DatabaseTable#from" when {
    "input is invalid" must {
      "return an empty Option" in {
        DatabaseTable.from(name = "", primaryKey = List("", "")) must be(empty)
      }
    }

    "input is valid" must {
      "return a DatabaseTable" in {
        forAll("name", "primary key") { (n: String, pk: List[String]) =>
          whenever(n.trim.nonEmpty && pk.forall(_.trim.nonEmpty)) {
            DatabaseTable.from(n, pk) match {
              case None => fail("Must create DatabaseTable from valid input!")
              case Some(t) =>
                t.name.show must be(n)
                t.primaryKey.map(_.show) must be(pk)
            }
          }
        }
      }
    }
  }

}
