/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors

import javax.xml.XMLConstants
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.stream.StreamSource
import javax.xml.validation.SchemaFactory

import cats.instances.string._
import cats.syntax.show._
import com.wegtam.tensei.schema.extractors.PrettyPrinterInstances._
import com.wegtam.tensei.schema.extractors.PrettyPrinter.ops._
import eu.timepit.refined.auto._
import eu.timepit.refined.cats._
import eu.timepit.refined.scalacheck.numeric._
import eu.timepit.refined.scalacheck.string._
import eu.timepit.refined.types.numeric._
import eu.timepit.refined.types.string.NonEmptyString
import org.dfasdl._
import org.scalacheck.{ Arbitrary, Gen }
import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.matchers.must.Matchers
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks
import com.wegtam.tensei.schema.extractors.databases.DatabaseTable

class DFASDLFunctionsTest extends AnyWordSpec with Matchers with ScalaCheckPropertyChecks {

  private val validCharacters: List[String] = ('a' to 'z')
    .map(_.toString)
    .toList ::: ('A' to 'Z').map(_.toString).toList ::: (0 to 9).map(_.toString).toList
  private val genValidName: Gen[NonEmptyString] =
    Gen.nonEmptyListOf(Gen.oneOf(validCharacters)).map(cs => NonEmptyString.unsafeFrom(cs.mkString))

  private val genEmptyDFASDLDocument: Gen[org.w3c.dom.Document] = Gen.delay {
    val b = {
      val xsd     = getClass().getResourceAsStream("/org/dfasdl/dfasdl.xsd")
      val factory = DocumentBuilderFactory.newInstance()
      factory.setValidating(false)
      factory.setNamespaceAware(true)
      val schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
      factory.setSchema(schemaFactory.newSchema(new StreamSource(xsd)))
      val builder = factory.newDocumentBuilder()
      builder
    }
    val document = b.newDocument()
    val dfasdl   = document.createElement(Element.ROOT.tagName)
    dfasdl.setAttribute("xmlns", "http://www.dfasdl.org/DFASDL")
    dfasdl.setAttribute(Attribute.SEMANTIC_SCHEMA.name, "custom")
    document.appendChild(dfasdl)
    document
  }
  private implicit val arbitraryEmptyDFASDLDocument: Arbitrary[org.w3c.dom.Document] = Arbitrary(
    genEmptyDFASDLDocument
  )

  private val genNumberOfNodes: Gen[PosInt]                      = Gen.choose(1, 32).map(PosInt.unsafeFrom)
  private implicit val arbitraryNumberOfNodes: Arbitrary[PosInt] = Arbitrary(genNumberOfNodes)

  private val genDatabaseTable: Gen[Option[DatabaseTable]] = for {
    name <- Gen.alphaNumStr
    pk   <- Gen.listOf(Gen.alphaNumStr)
  } yield DatabaseTable.from(name, pk.filterNot(_.isEmpty()))
  private implicit val arbitraryDatabaseTable: Arbitrary[Option[DatabaseTable]] = Arbitrary(
    genDatabaseTable
  )

  "Constant for ID attribute" must {
    "be correct" in {
      val expected: NonEmptyString = "id"
      DFASDLFunctions.ID_ATTR must be(expected)
    }
  }

  "Constant for DEFAULT_NUMERIC_SEPARATOR" must {
    "be correct" in {
      val expected: NonEmptyString = "."
      DFASDLFunctions.DEFAULT_NUMERIC_SEPARATOR must be(expected)
    }
  }

  "Constant for GENERAL_FORMATNUM_REGEX" must {
    "be correct" in {
      val expected: NonEmptyString = "(-?[\\d\\.,⎖]+)"
      DFASDLFunctions.GENERAL_FORMATNUM_REGEX must be(expected)
    }
  }

  "createFormatnumRegex" when {
    val separator: NonEmptyString = "."
    "scale is not set" when {
      "precision is not set" must {
        "return the GENERAL_FORMATNUM_REGEX" in {
          DFASDLFunctions.createFormatnumRegex(separator)(None)(None) must contain(
            DFASDLFunctions.GENERAL_FORMATNUM_REGEX
          )
        }
      }

      "precision is set" must {
        "return the correct regular expression" in {
          forAll("precision") { p: NonNegLong =>
            NonEmptyString.from(s"(-?\\d*?\\.\\d{0,${p.value.toString()}})") match {
              case Left(error) => fail(error)
              case Right(expected) =>
                DFASDLFunctions.createFormatnumRegex(separator)(None)(Option(p)) must contain(
                  expected
                )
            }
          }
        }
      }
    }

    "scale is set" when {
      "precision is not set" must {
        "return the correct regular expression" in {
          forAll("scale") { s: NonNegLong =>
            NonEmptyString.from(s"(-?\\d{1,${s.value.toString()}})") match {
              case Left(error) => fail(error)
              case Right(expected) =>
                DFASDLFunctions.createFormatnumRegex(separator)(Option(s))(None) must contain(
                  expected
                )
            }
          }
        }
      }

      "precision is set" must {
        "return the correct regular expression" in {
          forAll("scale", "precision") { (s: NonNegLong, p: NonNegLong) =>
            NonEmptyString
              .from(
                s"(-?\\d{0,${(s - p).toString()}}\\.\\d{0,${p.value.toString()}})"
              ) match {
              case Left(error) => fail(error)
              case Right(expected) =>
                DFASDLFunctions.createFormatnumRegex(separator)(Option(s))(Option(p)) must contain(
                  expected
                )
            }
          }
        }
      }
    }
  }

  "createTableSkeletonElement" when {
    "no exception is thrown" must {
      "return a correct table element" in {
        forAll("table") { to: Option[DatabaseTable] =>
          whenever(to.nonEmpty) {
            to.foreach { t =>
              val d = DFASDLFunctions.initDocument
              DFASDLFunctions.createTableSkeletonElement(d)(t) match {
                case None => fail("Must create a table skeleton element!")
                case Some(e) =>
                  e.getTagName() must be(Element.SEQUENCE.tagName.show)
                  if (t.primaryKey.nonEmpty) {
                    e.getAttribute(Attribute.DB_PRIMARY_KEY.name) must be(
                      t.primaryKey.mkString(",")
                    )
                  }
                  val row = e.getFirstChild()
                  row.getNodeName() must be(Element.ELEMENT.tagName.show)
                  row
                    .getAttributes()
                    .getNamedItem(DFASDLFunctions.ID_ATTR)
                    .getTextContent() must be(s"${t.name}_row")
              }
            }
          }
        }
      }
    }
  }

  "getFirstNode" when {
    "NodeList is empty" must {
      "return an empty Option" in {
        forAll("DFASDL Document") { d: org.w3c.dom.Document =>
          val nl = d.getDocumentElement().getChildNodes()
          whenever(nl.getLength() === 0) {
            DFASDLFunctions.getFirstNode(nl) must be(empty)
          }
        }
      }
    }

    "NodeList is not empty" must {
      "return the first element" in {
        forAll("DFASDL Document", "Number of Nodes") { (d: org.w3c.dom.Document, n: PosInt) =>
          val first = d.createElement(Element.ELEMENT.tagName)
          first.setAttribute("id", "FIRST-ELEMENT")
          d.getDocumentElement.appendChild(first)
          val expected = d.getDocumentElement().getChildNodes().item(0)
          for (c <- 1 until n) {
            val e = d.createElement(Element.ELEMENT.tagName)
            e.setAttribute("id", s"COUNTER-$c")
            d.getDocumentElement.appendChild(e)
          }
          withClue(s"Document must contain $n nodes!")(
            d.getDocumentElement().getChildNodes().getLength() must be(n.value)
          )
          DFASDLFunctions.getFirstNode(d.getDocumentElement().getChildNodes()) match {
            case None => fail("First node must be extracted!")
            case Some(f) =>
              withClue(s"Expected: ${expected.toPrettyString}")(
                f.toPrettyString must be(expected.toPrettyString)
              )
          }
        }
      }
    }
  }

  "initDocument" must {
    "return a properly initialised DFASDL document" in {
      val d = DFASDLFunctions.initDocument
      val r = d.getDocumentElement()
      r.getTagName() must be(Element.ROOT.tagName.show)
      r.getAttribute("xmlns") must be("http://www.dfasdl.org/DFASDL")
      r.getAttribute(Attribute.SEMANTIC_SCHEMA.name) must be("custom")
    }
  }

  "sanitizeName" when {
    "given name contains no special characters" must {
      "return the original input" in {
        forAll((genValidName, "name")) { n: NonEmptyString =>
          val name: String = n
          whenever(name.matches("^[\\w_-]*$")) {
            DFASDLFunctions.sanitizeName(n) match {
              case None    => fail("Function must return the original input!")
              case Some(r) => r must be(n)
            }
          }
        }
      }
    }

    "given name was output from sanitizeName" must {
      "return the original input" in {
        forAll("name") { n: NonEmptyString =>
          val sn = DFASDLFunctions.sanitizeName(n)
          sn match {
            case None    => succeed // Avoid "Gave up after X successful property evaluations."
            case Some(r) => DFASDLFunctions.sanitizeName(r) must be(sn)
          }
        }
      }
    }

    "given name contains special characters" must {
      "sanitize the name correctly" in {
        forAll("name") { n: NonEmptyString =>
          val expected = NonEmptyString.from(n.replaceAll("[^\\w_-]", "")).toOption
          DFASDLFunctions.sanitizeName(n) must be(expected)
        }
      }
    }
  }

}
