/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors.databases

import java.net.URI

import eu.timepit.refined.auto._
import org.scalacheck.Gen
import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.matchers.must.Matchers
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class DatabaseNameTest extends AnyWordSpec with Matchers with ScalaCheckPropertyChecks {

  private val invalidURI: Gen[URI]         = Gen.oneOf(List(new URI("")))
  private val expectedDbName: DatabaseName = "my-database"
  private val validURI: Gen[URI] = Gen.oneOf(
    List(
      new URI(s"jdbc:derby:/var/db/derby/$expectedDbName;create=true"),
      new URI(s"jdbc:firebirdsql://localhost:12345/$expectedDbName"),
      new URI(s"jdbc:firebirdsql://localhost/$expectedDbName"),
      new URI(s"jdbc:firebirdsql:$expectedDbName"),
      new URI(s"jdbc:h2:/var/db/h2/$expectedDbName"),
      new URI(s"jdbc:hsqldb:mem:$expectedDbName"),
      new URI(s"jdbc:hsqldb:file:/var/db/hsqldb/$expectedDbName"),
      new URI(s"jdbc:hsqldb:res:/com/wegtam/tensei/$expectedDbName"),
      new URI(s"jdbc:hsqldb:hsql://localhost/$expectedDbName"),
      new URI(s"jdbc:hsqldb:hsqls://127.0.0.1:12345/$expectedDbName"),
      new URI(s"jdbc:hsqldb:http://dbserver.example.com/$expectedDbName"),
      new URI(s"jdbc:hsqldb:https://dbserver.example.com/$expectedDbName"),
      new URI(s"jdbc:mariadb://localhost:12345/$expectedDbName"),
      new URI(s"jdbc:mysql://localhost:12345/$expectedDbName"),
      new URI(s"jdbc:oracle:oci:@$expectedDbName"),
      new URI(s"jdbc:oracle:oci8:@$expectedDbName"),
      new URI(s"jdbc:oracle:thin:@$expectedDbName"),
      new URI(s"jdbc:oracle:thin:@localhost:$expectedDbName"),
      new URI(s"jdbc:oracle:thin:@localhost:12345:$expectedDbName"),
      new URI(s"jdbc:postgresql://localhost:12345/$expectedDbName"),
      new URI(s"jdbc:postgresql://localhost:12345/$expectedDbName?user=fred&ssl=true"),
      new URI(s"jdbc:postgresql://localhost:12345/$expectedDbName?ssl=false"),
      new URI(s"jdbc:sqlite:/var/db/sqlite/$expectedDbName"),
      new URI(s"jdbc:sqlite::memory:$expectedDbName"),
      new URI(s"jdbc:sqlserver://localhost:12345;databaseName=$expectedDbName")
    )
  )
  private val validURIwithoutName: Gen[URI] = Gen.oneOf(
    List(
      new URI("jdbc:sqlserver://localhost:12345"),
      new URI("jdbc:whatever:")
    )
  )

  "fromURI" when {
    "scheme is empty" must {
      "return an error" in {
        forAll((invalidURI, "URI")) { u: URI =>
          withClue("Must fail on URI with empty scheme part!")(
            DatabaseName.fromURI(u).isLeft must be(true)
          )
        }
      }
    }

    "URI is valid and supported" must {
      "return the correct database name" in {
        forAll((validURI, "URI")) { u: URI =>
          DatabaseName.fromURI(u) match {
            case Left(e)  => fail(e)
            case Right(n) => n must be(expectedDbName)
          }
        }
      }
    }

    "URI is valid but missing the database name" must {
      "return an error" in {
        forAll((validURIwithoutName, "URI")) { u: URI =>
          withClue("Must fail on URI without database name!")(DatabaseName.fromURI(u))
        }
      }
    }
  }

}
