/*
 * Copyright (c) 2020 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.tensei.schema.extractors.databases

import cats.data.NonEmptyList
import cats.implicits._
import eu.timepit.refined.auto._
import eu.timepit.refined.cats._
import eu.timepit.refined.scalacheck.string._
import org.scalacheck.{ Arbitrary, Gen }
import org.scalatest.wordspec.AnyWordSpec
import org.scalatest.matchers.must.Matchers
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class CreateSQLStatementsTest extends AnyWordSpec with Matchers with ScalaCheckPropertyChecks {

  private val columnName: Gen[DatabaseColumnName] =
    Gen.alphaStr.suchThat(_.nonEmpty).map(DatabaseColumnName.unsafeFrom)
  private val columnNames: Gen[NonEmptyList[DatabaseColumnName]] =
    Gen.nonEmptyListOf(columnName).map(NonEmptyList.fromListUnsafe)
  private implicit val arbitraryColumnNames: Arbitrary[NonEmptyList[DatabaseColumnName]] =
    Arbitrary(columnNames)

  "createCountStatement" must {
    "return the correct statement" in {
      forAll("table name", "columns") { (n: DatabaseTableName, cs: NonEmptyList[DatabaseColumnName]) =>
        val columnString = cs.map(c => s"${c.value} = ?").toList.mkString(" AND ")
        val expected: DatabaseQueryString =
          DatabaseQueryString.unsafeFrom(s"SELECT COUNT(*) FROM ${n.show} WHERE $columnString")
        CreateSQLStatements.createCountStatement(n)(cs) must be(expected)
      }
    }
  }

  "createInsertStatement" must {
    "return the correct statement" in {
      forAll("table name", "columns") { (n: DatabaseTableName, cs: NonEmptyList[DatabaseColumnName]) =>
        whenever(cs.size > 1) {
          val autoIncCols = cs.toList.take(cs.size / 2)
          val valuePlaceholders =
            cs.map(c => if (autoIncCols.contains(c)) "DEFAULT" else "?").toList.mkString(", ")
          val expected =
            DatabaseQueryString.unsafeFrom(
              s"INSERT INTO ${n.show} (${cs.toList.mkString(", ")}) VALUES($valuePlaceholders)"
            )
          CreateSQLStatements.createInsertStatement(n)(cs)(autoIncCols) must be(expected)
        }
      }
    }
  }

  "createUpdateStatement" must {
    "return the correct statement" in {
      forAll("table name", "columns") { (n: DatabaseTableName, cs: NonEmptyList[DatabaseColumnName]) =>
        whenever(cs.size > 1) {
          val primaryKey       = NonEmptyList.fromListUnsafe(cs.toList.take(cs.size / 2))
          val columnString     = cs.map(c => s"${c.show} = ?").toList.mkString(", ")
          val primaryKeyString = primaryKey.map(c => s"${c.show} = ?").toList.mkString(" AND ")
          val expected =
            DatabaseQueryString.unsafeFrom(
              s"UPDATE ${n.show} SET $columnString WHERE $primaryKeyString"
            )
          CreateSQLStatements.createUpdateStatement(n)(cs)(primaryKey) must be(expected)
        }
      }
    }
  }

}
