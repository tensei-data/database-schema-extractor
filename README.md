# Tensei-Data Database Schema Extractor #

This library provides tooling to connect to a variety of databases and
extract schema and model information from it. It will generate a description
using the [DFASDL](https://github.com/DFASDL/dfasdl-core).

## Getting started

For development and testing you need to install [sbt](http://www.scala-sbt.org/).
Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details how to to contribute
to the project.

